
import React from 'react';

export default React.createClass({
  getInitialState() {
    return { draft: '' };
  },

  componentDidMount() {
    this.conversationChanged = this.conversationChanged.bind(this);
    this.props.conversation.addChangeHandler(this.conversationChanged);
    // gives us two ticks to first render... one should suffice.
    setTimeout(this.conversationChanged);
  },

  componentWillReceiveProps(nextProps) {
    this.props.conversation.removeChangeHandler(this.conversationChanged);
    nextProps.conversation.addChangeHandler(this.conversationChanged);
  },

  componentWillUnmount() {
    this.props.conversation.removeChangeHandler(this.conversationChanged);
  },

  conversationChanged() {
    setTimeout(() => {
      var messageLog = this.refs.messageLog.getDOMNode();
      messageLog.scrollTop = messageLog.scrollHeight;
    });
    this.forceUpdate();
  },

  render() {
    return (
      <div id="conversation">
        <button className="close" onClick={this.props.onClose}>&laquo;</button>
        <h1>{this.props.conversation.jid}</h1>
        <div className="message-log" ref='messageLog'>
          {this.props.conversation.backlog.map(this.renderMessage)}
        </div>
        {this.renderEditor()}
      </div>
    );
  },

  renderEditor() {
    var draft = this.state.draft;
    var newlines = draft.match(/\n/g);
    var lines = (newlines ? newlines.length : 0) + 1;
    return (
      <div className="editor">
        <textarea rows={lines}
                  value={draft}
                  onChange={this.draftChanged}
                  onKeyUp={this.interceptEditorKey} />
      </div>
    );
  },

  draftChanged(event) {
    if(event.target.value.slice(-1) !== '\n') {
      this.setState({ draft: event.target.value });
    }
  },

  interceptEditorKey(event) {
    if(event.which === 13 && (!event.ctrlKey)) {
      event.preventDefault();
      this.props.onSend(this.state.draft);
      this.setState({ draft: '' });
    } else if(event.which === 13) {
      this.setState({ draft: this.state.draft + '\n' });
    }
  },

  renderMessage(message) {
    return (
      <div className="message" key={message.date.getTime()}>
        <div className="avatar">
          <img src={message.from.avatar} />
        </div>
        <div className="from">{message.from.local}</div>
        <div className="time">{this.formatTime(message.date)}</div>
        <div className="body">{message.body}</div>
      </div>
    );
  },

  formatTime(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    return (hours >= 10 ? '' : '0') + 
      String(hours) + ':' +
      (minutes >= 10 ? '' : '0') +
      String(minutes);
  }
});
