
import React from 'react'

export default React.createClass({
  getInitialState() {
    return { controlsVisible: false };
  },

  render() {
    var name = this.props.jid ? this.props.jid.local : '...';
    var jid = this.props.jid ? this.props.jid.bare : '';
    var controlsVisible = this.state.controlsVisible;
    return (
      <div id="connection" data-state={this.props.state} title={jid}>
        <div className="icon" />
        <div className="name">{name}</div>
        <div className="toggle-controls"
             onClick={this.toggleControls}
             data-toggled={controlsVisible}>▾</div>
        {controlsVisible ? this.renderControls() : ''}
      </div>
    );
  },

  renderControls() {
    return (
      <div className="controls">
        <button onClick={this.props.onLogout}>Logout</button>
      </div>
    );
  },

  toggleControls() {
    this.setState({
      controlsVisible: !this.state.controlsVisible
    });
  }
});
