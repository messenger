
import React from 'react';

export default React.createClass({
  render: function() {
    var buddies = [];
    for(var key in this.props.items) {
      buddies.push(this.props.items[key]);
    }
    return (
      <div id="roster">
        <h>Contacts</h>
        {buddies.map(this.renderBuddy)}
      </div>
    );
  },

  renderBuddy: function(buddy) {
    var selected = this.props.selected === buddy.jid ? ' selected' : '';
    return (
      <div key={buddy.jid} className={'buddy' + selected}
           data-state={buddy.state}
           data-pending={buddy.pending > 0 ? 'true' : 'false'}
           data-jid={buddy.jid}
           onClick={this.handleBuddyClick}>
        <div className="icon" />
        <div className="name">{buddy.name}</div>
        {buddy.pending > 0 ?
          <div className="pending">{buddy.pending}</div>
         : ''}
      </div>
    );
  },

  handleBuddyClick: function(event) {
    var jid = event.target.dataset.jid || event.target.parentNode.dataset.jid;
    this.props.onBuddyClick(jid);
  }
});
