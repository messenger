
import React from 'react'
import XMPP from 'stanza.io';

import RosterView from './roster-view';
import ConversationView from './conversation-view';
import ConnectionView from './connection-view';
import AuthenticationView from './authentication-view';

import Conversation from './conversation';

const CLIENT_DEFAULTS = {
  transport: "websocket",
  wsURL: "ws://localhost:5280/xmpp-websocket",
  useStreamManagement: true
};

export default React.createClass({
  render() {
    return this.state.client ?
      this.renderAuthenticated()
    : <AuthenticationView {...CLIENT_DEFAULTS}
                          onAuthenticate={this.authenticate}
                          failed={this.state.authFailed} />
  },

  componentDidMount() {
    var savedState = this.loadSavedState();
    if(savedState.clientOptions) {
      this.authenticate(savedState.clientOptions);
    }
  },

  renderAuthenticated() {
    var conversation = this.state.currentConversation;
    var selectedJid = conversation ? conversation.jid : null;
    setTimeout(() => {
      document.body.dataset.panel = this.state.panel;
    });
    return (
      <div>
        <ConnectionView {...this.state.connection}
                        onLogout={this.logout} />
        <RosterView items={this.state.roster}
                    onBuddyClick={this.switchToConversation}
                    selected={selectedJid} />
        {conversation ?
           <ConversationView onSend={this.sendMessage}
                             onClose={this.switchToRoster}
                             conversation={conversation} />
         : ''}
      </div>
    );
  },

  getInitialState() {
    return {
      conversations: {},
      connection: {
        state: 'offline'
      },
      roster: {},
      panel: 'roster'
    };
  },

  sessionStarted() {
    var client = this.state.client;
    this.setConnState('online', client.jid);
    client.getRoster((err, response) => {
      var roster = {};
      response.roster.items.forEach(function(item) {
        var jid = item.jid.bare;
        roster[jid] = {
          name: item.jid.local,
          pending: 0,
          state: 'unknown',
          jid: jid
        };
      });
      this.setState({ roster: roster });
      client.sendPresence();
    });
  },

  receivedChat(chat) {
    this.lookupConversation(chat.from.bare, (conversation) => {
      chat.date = new Date();
      conversation.received(chat);
    });
  },

  receivedPresence(presence) {
    var buddy = this.state.roster[presence.from.bare];
    if(buddy) {
      buddy.state = presence.type;
      this.setState(this.state);
    }
  },

  switchToConversation(jid) {
    this.lookupConversation(jid, (conversation) => {
      this.setState({
        currentConversation: conversation,
        panel: 'conversation'
      });
    });
  },

  switchToRoster() {
    this.setState({
      panel: 'roster'
    });
  },

  lookupConversation(jid, callback) {
    if(this.state.conversations[jid]) {
      callback(this.state.conversations[jid]);
    } else {
      this.state.client.searchHistory({
        from: jid
      }, (err, history) => {
        var backlog = history.mamResult.items.map((item) => {
          return Object.assign(
            { date: item.forwarded.delay.stamp },
            item.forwarded.message
          );
        });
        var conversation = new Conversation(jid, backlog);
        this.state.conversations[jid] = conversation;
        callback(conversation);
      });
    }
  },

  setConnState(state, jid) {
    if(jid) {
      this.state.connection.jid = jid;
    }
    if(state) {
      this.state.connection.state = state;
    }
    this.setState({
      connection: this.state.connection
    });
  },

  sendMessage(body) {
    var conversation = this.state.currentConversation;
    var message = {
      to: conversation.jid,
      type: 'chat',
      body: body
    };
    this.state.client.sendMessage(message);
    message.from = this.state.client.jid;
    message.date = new Date();
    conversation.sent(message);
  },

  logout() {
    this.setState({ client: null });
    this.clearState({ clientOptions: true });
  },

  authenticate(clientOptions) {
    var client = XMPP.createClient(clientOptions);
    client.on('disconnected', () => this.setConnState('offline'));
    client.on('error', (e) => console.log('error!', e));
    client.on('connected', () => this.setConnState('connected', client.jid));
    client.on('session:started', this.sessionStarted);
    client.on('auth:failed', this.authFailed);
    client.on('chat', this.receivedChat);
    client.on('presence', this.receivedPresence);
    client.connect();
    this.setState({
      client: client,
      authFailed: false
    });
    this.setConnState('connecting', client.jid);

    this.saveState({ clientOptions: clientOptions });
  },

  authFailed() {
    this.setState({
      client: null,
      authFailed: true
    });
    this.clearState({ clientOptions: true });
  },

  saveState(object) {
    var savedState = this.loadSavedState();
    for(var key in object) {
      savedState[key] = object[key];
    }
    this.persistState(savedState);
  },

  clearState(object) {
    var savedState = this.loadSavedState();
    for(var key in object) {
      if(object[key]) {
        delete savedState[key];
      }
    }
    this.persistState(savedState);
  },

  loadSavedState() {
    var rawState = localStorage['saved-state'];
    var state;
    try {
      state = JSON.parse(rawState);
    } catch(e) {
      state = {};
    }
    return state;
  },

  persistState(object) {
    localStorage['saved-state'] = JSON.stringify(object);
  }
});
