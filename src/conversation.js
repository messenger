
export default class {
  constructor(jid, backlog) {
    this.jid = jid;
    this.backlog = backlog;
    this.changeHandlers = [];
  }

  addChangeHandler(handler) {
    this.changeHandlers.push(handler);
  }

  removeChangeHandler(handler) {
    this.changeHandlers = this.changeHandlers.filter(
      (h) => h !== handler
    )
  }

  received(message) {
    this.backlog.push(message);
    this.changed();
  }

  sent(message) {
    this.backlog.push(message);
    this.changed();
  }

  changed() {
    this.changeHandlers.forEach((handler) => handler());
  }
}
