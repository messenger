
import React from 'react'

export default React.createClass({
  getInitialState() {
    return { advanced: false };
  },

  render() {
    return (
      <form id="authentication" onSubmit={this.submitForm}>
        {this.renderError()}
        <input type="text"
               placeholder="JID"
               onChange={this.guessUsername}
               ref="jid" />
        <input type="password"
               placeholder="Password"
               ref="password" />
        <button type="submit">Login</button>
        <div className="toggle-advanced"
             onClick={this.toggleAdvanced}>
          {this.state.advanced ? '▾' : '▸'}
          Advanced
        </div>
        <div className="advanced" style={this.state.advanced ? {} : {display:'none'}}>
          <input type="text"
                 placeholder="Username"
                 value={this.state.username}
                 onChange={this.usernameChanged}
                 onBlur={this.guessUsername}
                 ref="username" />
          <input type="text"
                 placeholder="Websocket URL"
                 defaultValue={this.props.wsURL}
                 ref="wsURL" />
        </div>
      </form>
    );
  },

  toggleAdvanced() {
    this.setState({ advanced: ! this.state.advanced });
  },

  guessUsername() {
    var jid = this.getValue('jid');
    var update = { jid: jid };
    if(! this.state.usernameChanged) {
      update.username = jid.split('@')[0];
    }
    this.setState(update);
  },

  usernameChanged() {
    // username is equal to local part of JID, unless it was changed
    // manually. When the input is cleared, it goes back to default
    // behaviour.
    var username = this.getValue('username');
    if(username.length > 0) {
      this.setState({
        username: username,
        usernameChanged: true
      });
    } else {
      this.setState({
        usernameChanged: false,
        // this keeps the field cleared. Most likely the empty string
        // is not a valid username (in fact we count on it here), so
        // on blur jidChanged fills it back up. *Until* blur however,
        // usual editing behaviour is preserved.
        username: ''
      });
    }
  },

  submitForm(event) {
    event.preventDefault();
    this.props.onAuthenticate({
      jid: this.getValue('jid'),
      credentials: {
        username: this.getValue('username'),
        password: this.getValue('password')
      },
      wsURL: this.getValue('wsURL'),
      transport: this.props.transport,
      useStreamManagement: this.props.useStreamManagement
    });
  },

  getValue(key) {
    return this.refs[key].getDOMNode().value;
  },

  renderError() {
    if(this.props.failed) {
      return <div className="error">Authentication failed</div>;
    }
  }
});
