
var gulp = require('gulp');
var browserify = require('browserify');
var watchify = require('watchify');
var es6ify = require('es6ify');
var source = require('vinyl-source-stream');
var rename = require('gulp-rename');
var less = require('gulp-less');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefix = new LessPluginAutoPrefix({
  browsers: ["last 2 versions"]
});
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');

var reactify = require('./vendor/reactify');

var jsMainFile = './src/main';

var vendorFiles = [
    'node_modules/es6ify/node_modules/traceur/bin/traceur-runtime.js'];
var vendorBuild = 'build/vendor';
gulp.task('vendor', function () {
    return gulp.src(vendorFiles).
        pipe(gulp.dest(vendorBuild));
});

// Compile stylesheets into build/bundle.css
gulp.task('less', function() {
  return gulp.src('css/main.less').
    pipe(less({ plugins: [autoprefix] })).
    pipe(rename('bundle.css')).
    pipe(gulp.dest('build/'));
});

// Bundle JS / JSX files into build/bundle.js
gulp.task('build', function() {
  return bundle(browserify(jsMainFile));
});

gulp.task('all', ['build', 'less', 'vendor']);

// Continuously bundle JS / JSX files into build/bundle.js
gulp.task('watch', ['less', 'vendor'], function() {
  var watcher = watchify(jsMainFile);
  watcher.on('update', function(ids) {
    console.log('Update bundle due to change in: ', ids);
    return bundle(watcher);
  });
  watcher.on('bytes', function(bytes) {
    console.log('Done. ' + bytes + ' bytes written.');
  });
  return bundle(watcher);
});

function bundle(bundler) {
  return bundler.
    transform(reactify).
    transform(es6ify.configure(/\.jsx?/)).
    bundle({ debug: true }).
    pipe(plumber({ onError: notify.onError("Build failed: <%= error.message %>") })).
    pipe(source(jsMainFile)).
    pipe(rename('bundle.js')).
    pipe(gulp.dest('build/')).
    pipe(notify("Build done: <%= file.filename %>"));
}
